Rails.application.routes.draw do
 
 
  get 'static_pages/sandbox'

  root 'static_pages#intro'

  get '/game', to: 'static_pages#game'

  get '/repeated', to: 'static_pages#repeated'

  get '/representation', to: 'static_pages#representation'

  get '/representation2', to: 'static_pages#representation2'

  get '/cycle', to: 'static_pages#cycle'

  get '/payoff', to: 'static_pages#payoff'

  get '/run', to: 'static_pages#run'


end
