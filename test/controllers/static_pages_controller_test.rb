require 'test_helper'

class StaticPagesControllerTest < ActionDispatch::IntegrationTest
  test "should get sandbox" do
    get static_pages_sandbox_url
    assert_response :success
  end

end
