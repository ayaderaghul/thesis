<% provide(:image, "north.jpg") %>
<% provide(:author, "r.omas") %>


	    <header>V. Payoff Calculation </header>
	    <i> I would explain some theoretical stuff in this chapter. It is merely to describe how things are done in the field. I personally believe that the interpretations I am going to make next are very brave and a bit too far from reality. Anyway. </i>
	    <h1> Plain section 0 </h1>
	    <p> Think about the situation when we match two agents together for a game. The game can be one shot or it can be repeated for a number of rounds. If it is repeated for a number of rounds, the result is a payoff sequence for each agent. For example, the payoff sequence can be <code> 3 3 3 3 3 3...</code>How do we combine this sequence into a single number as the final payoff for the game? </p>
	    <p> Think about what the bank does. When you have a flow of cash over a period of time, they usually calculate the present value of that cash flow using some discount factor. We can think of the payoff sequence we get as a future cash flow and use a discount factor to calculate the present value of that flow. </p>

	    <p> To remind you a little bit about the ruthlessness of the financial market, here is how discount factor &delta; can be used on our payoff sequence: </p>
	    <pre class="game3">
<code> Round            0    1    2    3    4    5 ... </code>
<code> Payoff           3    3    3    3    3    3 ... </code>
<code> Discount factor  &delta;<sup>0</sup>   &delta;<sup>1</sup> </code>   &delta;<sup>2</sup>   &delta;<sup>3</sup>   &delta;<sup>4</sup>   &delta;<sup>5</sup>...</code>

<code> Present value = 3 + 3*&delta; + 3*&delta;<sup>2</sup> + ...</code>
	    </pre>
	    
	    <p> Now with the restriction that the discount factor &delta; can go from 0 to 1, we have the following cases: </p>
	    <ul>
		<li> In the case &delta; = 0; PV = 3 because all the payoffs that come after today are discounted to zero flat.  </li>
		<li> In the case &delta; = 1; PV = 3 + 3 + 3 ... which means that we simply sum up the payoff sequence. Because we regard the value of &euro;1 we get far in the future exactly the same as &euro;1 we get to hold today in our hand.</li>
		<li> In the case 0 < &delta; << 1 (which means that &delta; is small), as we go further into the payoff sequence, the present value of the payoffs would quickly drop. This is the case in which we are a bit myopic. Whoever calculate cash flows this way is someone who is very impatient. She like the money she can get today, and maybe tomorrow, but promises about stuff coming after tomorrow are considered noise to her.</li>
		<li> In the case &delta; is close to 1, as you can guess, the situation resembles someone who is highly patient. Payoff in the future is discounted a little but still bear value to her. To this person, even years from now, it is something still worth waiting for.</li>
	    </ul>
