# Purpose of the application
To animate my thesis in evolutionary game theory simulation

To practise Rails after reading [Rails Tutorial](https://www.railstutorial.org/book/frontmatter). I have learned a lot from [Learn Enough Society](https://www.learnenough.com/).

# License
I put it in the freest possible under local regulation. Which is the one of University of Trento, Italy. In the best case, you would get it in public domain under CC0.

# Getting started

Clone the repo and install:

```
$ bundle install --without production
```

Migrate the database:

```
$ rails db:migrate
```

Test:
```
$ rails test
```
Serve:
```
$ rails server
```
# Acknowledgement
The usual acknowledgement applies. These people significantly change the course of my life for the better: Romas Vijeikis, Hoàng Minh Thắng, Matthias Felleisen.
